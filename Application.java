public class Application {
	public static void main(String[] args) {
		Cat zeus = new Cat(); //Zeus is the name of my cat
		Cat yuki = new Cat(); //Yuki is the name of my (other) cat
		
		zeus.color = "brown";
		zeus.age = 8;
		zeus.sex = "male";
		
		yuki.color = "white";
		yuki.age = 6;
		yuki.sex = "male";
		
		Cat ca = new Cat();
		ca.petCat();
		ca.feedCat(zeus.color,yuki.color);
		
		Cat[] clowder = new Cat[3];
		clowder[0] = zeus;
		clowder[1] = yuki;
		clowder[2] = new Cat();
		
		clowder[2].color = "black";
		clowder[2].age = 7;
		clowder[2].sex = "female";
	}
}