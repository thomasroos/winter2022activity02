import java.util.*;
public class Cat {
	public String color;
	public int age;
	public String sex;
	
	public void petCat() {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Which cat would you like to pet? (Zeus[1] / Yuki[2]): ");
		int catChoice = scan.nextInt();
		
		if (catChoice == 1) {
			System.out.println("*You pet Zeus*");
			System.out.println("Zeus: mrrp meow meow");
		} else if (catChoice == 2) {
			System.out.println("*You pet Yuki*");
			System.out.println("Yuki: mew mrrp mew mew");
		} else {
			System.out.println("Incorrect Cat Choice");
		}
	}
	
	public void feedCat(String color1, String color2) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Which cat would you like to feed? (Zeus[1] / Yuki[2]): ");
		int catChoice = scan.nextInt();
		
		if (catChoice == 1) {
			System.out.println("*You feed Zeus*");
			System.out.println("*The big " + color1 + " cat is happy & fatter*");
			System.out.println("Zeus: mrrp meow meow");
		} else if (catChoice == 2) {
			System.out.println("*You feed Yuki*");
			System.out.println("*The big " + color2 + " cat is happy & fatter*");
			System.out.println("Yuki: mew mrrp mew mew");
		} else {
			System.out.println("Incorrect Cat Choice");
		}
	}
}